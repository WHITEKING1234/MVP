//
//  BeerPresenter.swift
//  MVP
//
//  Created by Mac on 25/3/23.
//

import Foundation

protocol BeerlistPresenter:AnyObject{
    init(view:BeerlistPresent,network:NetworkManagerprotocol)
    func getbeerlist()
    var getbeer:[BerrElement]? { get  set }

}


class BeerPresenter1:BeerlistPresenter {
    var getbeer: [BerrElement]?
    
    private weak var view:BeerlistPresent?
    private var network:NetworkManagerprotocol!
    required init(view: BeerlistPresent, network: NetworkManagerprotocol) {
        self.network = network
        self.view = view
        getbeerlist()
    }
    
    func getbeerlist() {
        func getBeerList() {
            network.getbeer { [weak self] result in
                guard let self = self else { return }
                DispatchQueue.main.async {
                    switch result {
                    case .success(let beers):
                        self.getbeer = beers
                        self.view?.recieve(beers: beers ?? [])
                    case .failure(let error):
                        self.view?.recieveError(error: error)
                        
                    }
                    
                    
                    
                    
                }
            }
        }
    }
}

