//
//  NetworkLayre.swift
//  MVP
//
//  Created by Mac on 27/3/23.
//

import Foundation
protocol NetworkManagerprotocol{
    func getbeer(comletion:@escaping(Result<[BerrElement]?,Error>) -> Void)
}
class NetworkManager:NetworkManagerprotocol{
    func getbeer(comletion: @escaping (Result<[BerrElement]?, Error>) -> Void) {
        let url = "https://api.punkapi.com/v2/beers"
        guard let url = URL(string: url) else {return}
        URLSession.shared.dataTask(with: url) { data, _, error in
            if let error = error{
                comletion(.failure(error))
                return
            }
            do{
                let obj = try JSONDecoder().decode([BerrElement].self, from: data!)
                comletion(.success(obj))
            }catch{
                comletion(.failure(error))
            }
        }.resume()
    }
    
    
}
